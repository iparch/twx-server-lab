## Purpose

Capture necassary containerize scripts and getting started information for the twx-server install in our IPA Digital Lab.  There will be a few different form factors used (work in progress):

1. Local docker-compose and kubernetes
2. Cloud DC/OS docker based install in PTC Cloud

## What is the IPA Digital Lab?

The Digital Lab is meant to capture the ongoing work to containerize & refactor various Industrial Innovation Products into an Integral Platform.  The architecture documents will be in our wiki (TODO: add link).

This sandbox environment is not production focused.  As refactors, redesigns, or new features mature out of the Lab, we will migrate them into their respective parent product directories.  Think of this as the "nightly" build of the PTC Industrial Innovation Platform.

---

# Getting Started

---

## Clone the repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You�ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you�d like to and then click **Clone**.
4. Open the directory you just created to see your repository�s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).

## Setting up dependent repos

You will notice that the files contained in the repo is minimal.  This is so you use the various repos that already exist to be the source code for your docker containers.  The naming convention is very straightforward:

twx-server-lab contains the container build scripts and Dockerfiles needed for the tw-server repo.  (Sorry for the extra 'x')

Thus, for this repo, make sure to git clone https://bitbucket.org/thingworx-ondemand/tw-server/overview



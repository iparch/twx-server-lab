#!/bin/bash -e

# copy over other WAR files to tomcat (if there are any)
# for example, there might be a Help Center WAR file
copy_war_files_to_tomcat() {
  FILES=$(find /maven -maxdepth 1 -type f -iname '*.war')
  if [ -n "$FILES" ]; then
    echo "Copying WAR files to tomcat webapps..."
    while read -r i; do
      echo "Copying $i to tomcat webapps";
      cp $i /usr/local/tomcat/webapps
    done <<< "$FILES"
  fi
}

call_script() {
  echo "Calling script $1"
  chmod +x "$1"
  sync # no idea why I have to sync here, but I ger an error about the file being busy w/o it
  eval "$1"
  if [ $? -ne 0 ]; then
    echo "Non-zero exit code from $1; aborting"
    exit 1
  fi
}

run_config_scripts_single() { #extension-zip, [pre-import|post-import|tomcat-conf]
  local extension="${1}"
  local scripts="${2}"

  if [[ -n "$(unzip -Z -1 "${extension}" | grep "^twx-config/$")" ]]; then
    echo "${extension} has configuration info, running config scripts"
    mkdir /tmp/ext-config
    unzip ${extension} -d /tmp/ext-config
    pushd /tmp/ext-config/twx-config

    #ensure required params are set in the environment
    for i in $(jq -r '.params | map(select(.required == true)) | .[] | .name' config.json); do
      if [[ -z "$(eval echo \$${i})" ]]; then
        echo "Required param ${i} for ${extension} not set in environment"
        exit 1
      fi
    done

    #set default params for unset vars
    for i in $(jq -r '.params | map(select(has("defaultValue"))) | .[] | .name' config.json); do
      local defaultValue=$(jq -r --arg name ${i} '.params | map(select(.name == $name)) | .[] | .defaultValue' config.json)
      if [[ -z "$(eval echo \$${i})" ]]; then
        eval "${i}=\${defaultValue}"
      fi
    done

    #export params for use in script
    for i in $(jq -r '.params | .[] | .name' config.json); do
      eval export ${i}
    done

    for i in $(jq -r --arg scripts ${scripts} '.scripts | .[$scripts] | .[]' config.json); do
      call_script "./${i}"
    done
    popd
    rm -rf /tmp/ext-config
  else
    echo "${extension} has no configuration info, skipping"
  fi
}

run_config_scripts() { #extension-zip, [pre-import|post-import|tomcat-conf]
  local extension=${1}
  local scripts=${2}

  # Are we dealing with a zip of zips or an extension
  local is_uber_zip=true
  local extension_contents=$(unzip -Z -1 "${extension}")
  for i in ${extension_contents}; do
    if [[ -z "$(echo ${i} | grep "\.zip$")" ]]; then
      is_uber_zip=false
      break
    fi
  done

  if ${is_uber_zip}; then
    mkdir /tmp/uber-unpack;
    unzip ${extension} -d /tmp/uber-unpack
    for i in ${extension_contents}; do
      run_config_scripts_single "/tmp/uber-unpack/${i}" "${scripts}"
    done
    rm -rf /tmp/uber-unpack
  else
    run_config_scripts_single "${extension}" "${scripts}"
  fi
}

load_twx_extension_package() {
  FILE_TO_LOAD="$1"

  echo "Running pre-import configuration scripts for ${FILE_TO_LOAD}"
  run_config_scripts "${FILE_TO_LOAD}" "pre-import"

  echo "Loading extension ${FILE_TO_LOAD}";
  HTTP_STATUS_CODE=$(curl -s -F "file=@${FILE_TO_LOAD}" -u "Administrator:trUf6yuz2?_Gub" -H "Accept: application/json" \
      -o /tmp/twx_load_output -w "%{http_code}" \
      -H "X-XSRF-TOKEN:TWX-XSRF-TOKEN-VALUE" http://localhost:8080/Thingworx/ExtensionPackageUploader?purpose=import;)
  if [ "200" != "${HTTP_STATUS_CODE}" ]; then
    echo "Failed to load extension ${FILE_TO_LOAD}:"
    grep "ERROR" /ThingworxStorage/logs/ErrorLog.log
    cat /tmp/twx_load_output
    exit 1
  fi

  echo "Running post-import configuration scripts for ${FILE_TO_LOAD}"
  run_config_scripts "${FILE_TO_LOAD}" "post-import"
}

load_twx_entity_import() {
  FILE_TO_LOAD="$1"


  echo "Importing entity file ${FILE_TO_LOAD}"
  HTTP_STATUS_CODE=$(curl -s -F "file=@${FILE_TO_LOAD}" -u "Administrator:trUf6yuz2?_Gub" -H "Accept: application/json" \
      -o /tmp/twx_load_output -w "%{http_code}" \
      -H "X-XSRF-TOKEN:TWX-XSRF-TOKEN-VALUE" http://localhost:8080/Thingworx/Importer?purpose=import;)
  if [ "200" != "${HTTP_STATUS_CODE}" ]; then
    echo "Failed to import file ${FILE_TO_LOAD}:"
    cat /tmp/twx_load_output
    exit 1
  fi
}

load_twx_extensions() {
  echo "Loading extensions and imports";
  if [ ! -z "${LOAD_ORDER}" ]; then
    for i in ${LOAD_ORDER}; do
      FILE_EXTENSION="${i##*.}"
      if [ "${FILE_EXTENSION}" = "xml" ]; then
        load_twx_entity_import "/maven/$i"
      elif [ "${FILE_EXTENSION}" = "zip" ]; then
        load_twx_extension_package "/maven/$i"
      else
        echo "Invalid load file ${i}";
        exit 1;
      fi
    done
  else
    if [ ! -z "$EXTENSION_LOAD_ORDER" ]; then
      EXTENSIONS="";
      for i in ${EXTENSION_LOAD_ORDER}; do
        EXTENSIONS="${EXTENSIONS} /maven/${i}";
      done
    else
      EXTENSIONS=`find /maven -maxdepth 1 -type f -iname '*.zip'`;
    fi
    for i in ${EXTENSIONS}; do
      load_twx_extension_package "$i"
    done
    if [ ! -z "$ENTITY_LOAD_ORDER" ]; then
      ENTITIES="";
      for i in ${ENTITY_LOAD_ORDER}; do
        ENTITIES="${ENTITIES} /maven/${i}";
      done
    else
      ENTITIES=`find /maven -maxdepth 1 -type f -iname '*.xml'`;
    fi
    for i in ${ENTITIES}; do
      load_twx_entity_import "$i"
    done
  fi
}

tomcat_config_scripts() {
  local extensions=$(find /maven -maxdepth 1 -type f -iname '*.zip')
  for i in ${extensions}; do
    echo "Running tomcat-conf scripts for ${i}"
    run_config_scripts "${i}" "tomcat-conf"
  done
}

configure_twx_persistence_provider() {
  mkdir /usr/local/tomcat/webapps/Thingworx

  echo "starting persistence provider";

  echo "${POSTGRES_PORT_5432_TCP}";

  # if env | grep -q POSTGRES_PORT_5432_TCP ; then
    # we are running postgres
    unzip -qq /twx-postgres.war -d /usr/local/tomcat/webapps/Thingworx
    if [ ! -f /ThingworxPlatform/platform-settings.json ]; then
      cat > /ThingworxPlatform/platform-settings.json << HEREDOC
{
	"PersistenceProviderPackageConfigs": {
		"PostgresPersistenceProviderPackage": {
			"ConnectionInformation": {
				"jdbcUrl": "jdbc:postgresql://${POSTGRES_PORT_5432_TCP_ADDR}:${POSTGRES_PORT_5432_TCP_PORT}/thingworx",
				"password": "${POSTGRES_PASS:-password}",
				"username": "${POSTGRES_USER:-twadmin}"
			}
		}
	}
}
HEREDOC
    fi
  # elif env | grep -q TWX_H2_PERSISTENCE ; then
  #   # we are running h2
  #   unzip -qq /twx-h2.war -d /usr/local/tomcat/webapps/Thingworx
  # else
  #   # we are running neo
  #   unzip -qq /twx-neo.war -d /usr/local/tomcat/webapps/Thingworx
  # fi
}

################################################################################
# entry point                                                                  #
################################################################################
if [ "$1" = "catalina.sh" ]; then

  #Initialize environment if it exists
  if [ -e /maven/environment.sh ]; then
     chmod +x /maven/environment.sh
     source /maven/environment.sh
  fi

  # use UTF-8 as the default locale/encoding; TWX expects this. Otherwise you
  # can't have non-ASCII filenames (that people might upload)
  # TWX wants the file encoding to be UTF8
  export JAVA_OPTS="$JAVA_OPTS -Dfile.encoding=UTF-8 -Djava.library.path=/usr/local/tomcat/webapps/Thingworx/WEB-INF/extensions"

  echo "Starting ThingWorx";

  # if /ThingworxStorage is empty, the container is being ran for the first
  # time; do some initialization
  if [ -z "$(ls -A /ThingworxStorage)" ]; then
    echo "Initializing ThingWorx";
  
    /configure_ssl.sh

    configure_twx_persistence_provider

    if [ -d /maven ]; then
      copy_war_files_to_tomcat

      echo "Starting ThingWorx";
      catalina.sh start;
      if [ $? != 0 ]; then
        cat /usr/local/tomcat/bin/catalina.out;
        exit;
      fi

      while [ $(curl -s -o /dev/null -w "%{http_code}" -u "Administrator:trUf6yuz2?_Gub" http://localhost:8080/Thingworx/Composer/index.html) != "200" ]; do sleep 1; done;

      load_twx_extensions

      echo "Shutting down ThingWorx";
      catalina.sh stop;
      while pgrep java > /dev/null; do sleep 1; done;

      tomcat_config_scripts

      echo "Initialization Finished";
    fi
  fi
fi

exec "$@"

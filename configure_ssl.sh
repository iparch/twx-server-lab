#!/bin/bash -eu

# See note below about ssl security.
CIPHERS='ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:AES128-SHA:AES256-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!3DES:!MD5:!PSK:!DHE'

TOMCAT_CONF=${CATALINA_HOME}/conf/server.xml
PROBLEM=0
NO_BUNDLE=0

if [ ! -f "${TOMCAT_CONF}" ] ; then
    cd /usr/local/tomcat/conf
    ls
    echo "Cannot configure Tomcat SSL, cannot locate server.xml (given [${TOMCAT_CONF}])"
    PROBLEM=1
fi

if [ -z "${JAVA_HOME}" ] ; then
    echo "Cannot enable Tomcat SSL without having JAVA_HOME set."
    PROBLEM=1
else
    PATH="${PATH}:${JAVA_HOME}/bin"
fi

if [ -d /certs ] ; then
    if [ ! -f /certs/host.crt ] ; then
        echo "Cannot configure Tomcat SSL, missing host.crt!"
        PROBLEM=1
    fi

    if [ ! -f /certs/host.key ] ; then
        echo "Cannot configure Tomcat SSL, missing host.key!"
        PROBLEM=1
    fi

    if [ ! -f /certs/ca-bundle.crt ] ; then
        echo "Assuming host.crt contains all intermediary certificates!"
        NO_BUNDLE=1
    fi
else
    PROBLEM=1
fi


CONNECTOR_EXISTS=$(xmlstarlet sel -t -v 'count(/Server/Service[@name="Catalina"]/Connector[@port="8443"])' "$TOMCAT_CONF")

if [ $CONNECTOR_EXISTS -gt 0 ] ; then
    echo "Tomcat already configured for SSL."
    exit 0;
fi

if [ $PROBLEM -eq 0 ] ; then
    echo "Configuring Tomcat for SSL."

    echo "Converting certificates to a PKCS12 file"
    # Convert the given keys into a PKCS12 file
    if [ $NO_BUNDLE -eq 0 ]; then
        cat /certs/host.crt /certs/ca-bundle.crt > /tmp/combinedHostAndCA.crt
    else
        # In the case there is no ca-bundle.crt; then just assume that all the certificates are available in host.crt already
        cp /certs/host.crt /tmp/combinedHostAndCA.crt
    fi

    openssl pkcs12 -export -in /tmp/combinedHostAndCA.crt -inkey /certs/host.key \
            -out /tmp/server.p12 -name tomcat \
            -passout pass:changeit

    echo "Importing PKCS12 file into a new keystore"
    # Import the PKCS12 file into a new keystore.
    keytool -importkeystore \
            -deststorepass changeit -destkeypass changeit \
            -destkeystore /usr/local/tomcat/tomcat.keystore \
            -srckeystore /tmp/server.p12 \
            -srcstoretype PKCS12 \
            -srcstorepass changeit \
            -alias tomcat

    # Remove the intermediary files
    rm -f /tmp/combinedHostAndCA.crt
    rm -f /tmp/server.p12



    echo "Modifying tomcat's server.xml"
    # Note, the security strength of the below settings was tested by https://www.ssllabs.com/ssltest/index.html and rated at
    # Overall Rating "A"
    # Certificate: 100
    # Protocol Support: 95
    # Key Exchange: 90
    # Cipher Strength: 90

    # An outstanding problem is that JSSE does not support TLS_FALLBACK_SCSV at the time of authoring this script (Java 1.8u51)

    # Modify Tomcat's server.xml file using XMLStarlet
    # Inserting a new element with attributes cannot be done directly.
    # Instead we insert an element with a unique name "CaseyTempElement"
    # Add attributes to that unique element
    # Rename that unique element to what it is supposed to be.
    xmlstarlet ed --inplace \
               -a '/Server/Service/Connector[@port="8080"]' -t elem -n 'CaseyTempElement' -v "" \
               -i //CaseyTempElement -t attr -n "port" -v "8443" \
               -i //CaseyTempElement -t attr -n "protocol" -v "org.apache.coyote.http11.Http11NioProtocol" \
               -i //CaseyTempElement -t attr -n "maxThreads" -v "150" \
               -i //CaseyTempElement -t attr -n "SSLEnabled" -v "true" \
               -i //CaseyTempElement -t attr -n "scheme" -v "https" \
               -i //CaseyTempElement -t attr -n "secure" -v "true" \
               -i //CaseyTempElement -t attr -n "keystoreFile" -v "/usr/local/tomcat/tomcat.keystore" \
               -i //CaseyTempElement -t attr -n "keystorePass" -v "changeit" \
               -i //CaseyTempElement -t attr -n "clientAuth" -v "false" \
               -i //CaseyTempElement -t attr -n "sslProtocol" -v "TLS" \
               -i //CaseyTempElement -t attr -n "ciphers" -v "${CIPHERS}" \
               -i //CaseyTempElement -t attr -n "useServerCipherSuitesOrder" -v "true" \
               -i //CaseyTempElement -t attr -n "sslEnabledProtocols" -v "TLSv1,TLSv1.1,TLSv1.2" \
               -r //CaseyTempElement -v "Connector" \
               "${TOMCAT_CONF}"
else
    echo "No certificates available; not configuring Tomcat for SSL."
    echo "If SSL support is desired, bind mount /certs to a directory containing host.crt host.key and optionally ca-bundle.crt"
    echo "If ca-bundle.crt is not provided then it is assumed that host.crt contains all certificates up to a trusted root authority."
fi

ThingWorx Docker Image
======================

Docker image of ThingWorx for development and testing. In addition to running ThingWorx this image
will also look at the content of the directory `/maven` and install extensions and import entity files
it finds there.

Usage
-----

To simply start a ThingWorx server, use the command:

```
docker run -p 8080:8080 -d --name thingworx cpmdevops:5000/thingworx
```

To start a ThingWorx server that automatically loads a set of extensions present
in your `/home/extensions` directory use the command:

```
docker run -p 8080:8080 -v /home/extensions:/maven --name thingworx cpmdevops:5000/thingworx
```

**NOTE: Any .war files included in `/maven` within the container, will be copied to $CATALINA_HOME/webapps and deployed.**

If you have multiple extensions/entity files in your `/maven` directory that have dependencies on each other, you
can use the env variables `EXTENSION_LOAD_ORDER` and `ENTITY_LOAD_ORDER` to specify a space separated load order. For
example, if your `/load` directory contains ExtA.zip and ExtB.zip and ExtA depends on ExtB you can use the following
command to load the extensions correctly:

```
docker run -p 8080:8080 -v /load:/maven -e "EXTENSION_LOAD_ORDER=ExtB.zip ExtA.zip" cpmdevops:5000/thingworx
```

This image also exposes the standard Tomcat jpda debugging port. To make ThingWorx accessible to a
debugger, use the following options and command:

```
docker run -p 8080:8080 -p 8000:8000 cpmdevops:5000/thingworx catalina.sh jpda run
```

Logback
-------

The docker image includes a custom `/ThingworxPlatform/logback.xml` file. This sets default log level for `com.ptc`
to INFO to match default for `com.thingworx`. It also creates console appenders for all Thingworx logs and uses a
layout pattern that includes exception stack traces. This should allow you to see all logs directly with
`docker logs` command (assuming `thingworx` is the nice name of the container):

```
docker logs -f thingworx
```

All logs are still also written out to their normal files within `/ThingworxStorage/logs`.

Log levels are still primarily controllable through Thingworx's log monitoring mashups. However, since setting level
on ApplicationLog only actually changes the level of the `com.thingworx` logger, the custom logback config also includes
support for changing the default level of root, `com`, `com.ptc`, and `org` based on a `LOG_LEVEL` environment variable which
can be specified on `docker run`:

```
docker run -e LOG_LEVEL=DEBUG ...
```

For more information on customization of logback, see the LOGBACK.md file.

Storage Engine
--------------

The container will detect a PostgreSQL database if it is linked into the container as 'postgres' and use that as the
storage backend. It will attempt to connect to the database using the user 'twadmin' with password 'password'; these
values can be overridden with the environment variables `POSTGRES_USER` and `POSTGRES_PASS`

If no database is linked in to the container, neo4j will be used as the persistence provider.


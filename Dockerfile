FROM tomcat:8.5

# use UTF-8 as the default locale/encoding; TWX expects this. Otherwise you
# can't have non-ASCII filenames (that people might upload)
ENV LANG=C.UTF-8

VOLUME ["/ThingworxStorage", "/ThingworxBackupStorage"]

# needed by run-thingworx.sh script
RUN apt-get update && apt-get install -y xmlstarlet unzip jq openssl procps && apt-get clean && rm -rf /var/lib/apt/lists/*

# Needed for "dockerize" which enables us to wait for specific tcp ports to be available (Postgres:5432)
ENV DOCKERIZE_VERSION v0.6.0
RUN wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz

# RUN rm -rf $CATALINA_HOME/webapps/*
COPY ./conf/logging.properties $CATALINA_HOME/conf/logging.properties
COPY ./conf/server.xml $CATALINA_HOME/conf/server.xml

COPY license.bin logback.xml /ThingworxPlatform/
COPY ./dist/twx-*.war /
COPY configure_ssl.sh run-thingworx.sh /

EXPOSE 8000
EXPOSE 8443
EXPOSE 8080

# ENTRYPOINT ["/run-thingworx.sh"]
CMD dockerize -wait tcp://postgres:5432 /run-thingworx.sh catalina.sh run
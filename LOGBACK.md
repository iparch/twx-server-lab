Logback Customization
=====================

As introduced in the main readme, this container includes a custom `ThingworxPlatform/logback.xml` which provides:

* console logging for docker which additionally uses a message layout that includes exception stack traces
* set `com.ptc` logger level to INFO by default rather than the WARN level inherited from `com`
* allow controlling log level of root, `com`, `com.ptc`, and `org` through a `LOG_LEVEL` environment variable
* allow inclusion based customization through a `/ThingworxPlatform/logback-include.xml` file
* allow wholesale based customization through overriding the `/ThingworxPlatform/logback.xml` completely

Setting Levels through Thingworx Services
-----------------------------------------

The Thingworx logging monitor mashup lets you set the log level of each log type - ApplicationLog being the one we're
usually most concerned with, since its the root logger. However, the SetLogLevel service that is called treats setting
the level for ApplicationLog specially - actually only changing the level for the `com.thingworx` logger. This is why
changing the level through the mashup never affects any logs produced by `com.ptc.*` loggers.

Luckily, there is a second service available which does allow setting level for a specific named logger. By using
`SetSubLoggerLevel` service we can at runtime change the level of logger. Log entities don't show up in the composer
but they are available through the REST apis, so setting `com.ptc` to DEBUG dynamically could look like this in a
browser:

```
http://example.com/Thingworx/Logs/ApplicationLog/Services/SetSubLoggerLevel?method=POST&sublogger=com.ptc&level=DEBUG
```

Or you could use something like curl from the command line

```
curl -sS http://example.com/Thingworx/Logs/ApplicationLog/Services/SetSubLoggerLevel --data "sublogger=com.ptc&level=DEBUG"
```

Using a logback-include.xml File
--------------------------------

The logback.xml file we use has the following:

    <include optional="true" file="/ThingworxPlatform/logback-include.xml"/>

Which means you can mount your own logback-include.xml file and have it be an additive config to what we've already
provided. Much simpler when you only want to change a few things. The only restriction is that logback inclusions have
to have the root xml element of `<included>` rather than `<configuration>`. See the section on configuring Beagle to
see an example of an included file and how to mount it into the docker container.


Replacing the logback.xml File
------------------------------

If there are parts of the logback.xml we've included that you don't like at all, you can replace it entirely by using
dockers volume options:

```
docker run -v my-custom-logback.xml:/ThingworxPlatform/logback.xml ...
```

In fact, you could replace the entire /ThingworxPlatform directory with your own mount this way.


Using Beagle with Logback
-------------------------

[Beagle](http://logback.qos.ch/beagle/) is an eclipse plugin that lets you receive remote log messages directly in the
IDE. Additionally, it provides filtering and source code click through to available source for any stack traces.

The plugin acts as a tcp server. In order to receive log messages, you will need to configure your remote application
to connect to the appropriate host and port. If you're running docker inside of a VM this means taking extra steps to
find the IP the guest can use to access the host. Usually a virtualbox config with a private only network has no problem
NAT'ing to the main IP of the host.

To customize your logback configuration you'll want to override the entire logback.xml file or create a logback
inclusion file such as the following:

    <included>
      <appender name="BEAGLE" class="ch.qos.logback.classic.net.SocketAppender">
        <!-- the beagle plugin binds to all ips of the host, pick one that docker container can reach -->
        <remoteHost>192.168.0.4</remoteHost>
        <!-- default beagle plugin server port -->
        <port>4321</port>
        <!-- number of log messages to queue if the beagle server is slow to receive -->
        <queueSize>500</queueSize>
      </appender>

      <!-- you'll always want to add the appender to root at least, you may also want the other logs as well, see the logback.xml file -->
      <root>
        <appender-ref ref="BEAGLE"/>
      </root>
    </included>

Assuming the file you create is named my-beagle-include.xml, you can then mount it into the thingworx container during
run to get it loaded:

```
docker run ... -v my-beagle-include.xml:/ThingworxPlatform/logback-include.xml ...
```
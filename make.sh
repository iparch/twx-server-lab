#!/bin/bash -e

# TWX_REPO="$1"
# if [ -z "$TWX_REPO" ]; then
#   TWX_REPO="twxlibs-snapshot-local"
# fi

TW_REPO=/Users/Michael/Desktop/lab/thingworx/tw-server
TWX_LAB=$(pwd)

TWX_REPO="$2"
if [ -z "$TWX_REPO" ]; then
  TWX_REPO="twxlibs-snapshot-local"
fi

# download license.bin
if [ ! -f license.bin ]; then
  echo "downloading license.bin..."
  curl -u "${ARTIFACTORY_USERNAME}:${ARTIFACTORY_KEY}" -X GET -o license.bin https://artifactory.rd2.thingworx.io/artifactory/${TWX_REPO}/com/thingworx/flexnet/licenses/1.0/internalLicense/8.2/license.bin
fi

# # download neo war
# if [ ! -f twx-neo.war ]; then
#   echo "downloading twx-neo.war..."
#   curl -u "${ARTIFACTORY_USER}:${ARTIFACTORY_PASSWORD}" -X GET -o twx-neo.zip https://artifactory.rd2.thingworx.io/artifactory/${TWX_REPO}/com/thingworx/Thingworx-Platform-Neo/${TWX_VERSION}/Thingworx-Platform-Neo-${TWX_VERSION}.zip
#   unzip -qp twx-neo.zip Thingworx.war > twx-neo.war
#   rm twx-neo.zip
# fi

# run gradle build and copy postgres war to dist folder
if [ ! -f $TWX_LAB/dist/twx-postgres.war ]; then
  echo "running gradle build for twx-postgres.war..."
  cd $TW_REPO
  ./gradlew :thingworx-platform-postgres:build
  echo "unzipping twx-postgres.war..."
  cd $TW_REPO/thingworx-platform-postgres/build/distributions
  unzip -qp Thingworx-Platform*.zip Thingworx.war > twx-postgres.war
  echo "copying war file to dist folder..."
  cp twx-postgres.war $TWX_LAB/dist
fi

# # download H2 war
# if [ ! -f twx-h2.war ]; then
#    echo "downloading twx-h2.war..."
#   curl -u "${ARTIFACTORY_USER}:${ARTIFACTORY_PASSWORD}" -X GET -o twx-h2.zip https://artifactory.rd2.thingworx.io/artifactory/${TWX_REPO}/com/thingworx/Thingworx-Platform-H2/${TWX_VERSION}/Thingworx-Platform-H2-${TWX_VERSION}.zip
#   unzip -qp twx-h2.zip Thingworx.war > twx-h2.war
#   rm twx-h2.zip
# fi

# docker-compose -f docker-compose-lab.yml build
# docker-compose -f docker-compose-lab.yml up

# rm -rf twx-neo.war twx-postgres.war twx-h2.war license.bin
